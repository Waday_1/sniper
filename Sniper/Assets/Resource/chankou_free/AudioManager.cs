﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour {

	[SerializeField] AudioSource cheerSE1;
	[SerializeField] AudioSource cheerSE2;
	[SerializeField] AudioSource shotSE;

	public static AudioManager Instance {
		get; private set;
	}

	void Awake() {
		if (Instance != null) {
			Destroy(gameObject);
			return;
		}
		Instance = this;
		DontDestroyOnLoad (gameObject);
	}

	void Start () {
		
	}

	public void PlayCheerSE1 () {
		cheerSE1.Play ();
	}

	public void PlayCheerSE2 () {
		cheerSE2.Play ();
	}

	public void PlayShotSE () {
		shotSE.Play ();
	}
}
