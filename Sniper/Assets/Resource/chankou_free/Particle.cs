﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Particle : MonoBehaviour {

	int blockNumber = 20;
	[SerializeField] GameObject blockPrefab;
	GameObject[] blocks;
	public Material material;

	// Use this for initialization
	void Start () {
		StartCoroutine (DestroyCoroutine ());
	}

	// posの位置から弾ける
	public void Burst (Vector3 pos) {
		blocks = new GameObject[blockNumber];
		for (int i = 0; i < blocks.Length; i++) {
			//blocks [i].SetActive (true);
			// ブロックを生成
			GameObject obj = (GameObject)Instantiate (
				blockPrefab,
				pos,
				Quaternion.identity
			);
			obj.transform.parent = this.transform;
			blocks [i] = obj;

			// マテリアルをセット
			obj.GetComponent<MeshRenderer> ().material = material;

			// 弾ける
			float xPower = Random.Range (-100f, 100f);
			float yPower = Random.Range (0f, 300f);
			float size = Random.Range (0.1f, 0.3f);
			obj.transform.localScale = Vector3.one * size;
			Rigidbody rb = blocks [i].GetComponent<Rigidbody> ();
			rb.AddForce (new Vector3(xPower, yPower, 0f));
		}
	}

	// 3秒後に破棄
	IEnumerator DestroyCoroutine () {
		float waitTime = 3f;
		yield return new WaitForSeconds (waitTime);
		Destroy (this.gameObject);
	}
}
