﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour {

    public GameObject[] EnemyList;
    public float[] NextTime;

    public int currentWave;
    public bool isNext;
    public float currentTime;
    bool isClear;

    public bool IsClear
    {
        get
        {
            return isClear;
        }
    }

    // Use this for initialization
    void Start() {
    }

#if false
    public int stageIndex;

    // Update is called once per frame
    void Update()
    {
        if (isCleare == false)
        {
            switch (stageIndex)
            {
                case 1:
                    Stage1();
                    if (isNext==false)
                    {
                        isNext = isEnemyDeath();
                    }
                    break;
            }
        }
    }

    void Stage1()
    {
        if(isNext)
        {
            currentTime += Time.deltaTime;

            // Wave間のCoolタイムを調査
            if (currentWave < NextTime.Length && currentTime <= NextTime[currentWave])
                return;

            // 何のために…？
            Vector2 pos = new Vector2(gameObject.transform.position.x, gameObject.transform.position.y);

            // プレハブからインスタンスを生成
            GameObject obj = (GameObject)Instantiate(EnemyList[currentWave], transform.position, Quaternion.identity);

            // 作成したオブジェクトを子として登録
            obj.transform.parent = transform;
            isNext = false;

        }
    }
#else
    void Update()
    {
        if (isClear == false)
        {
            GenerateWave();
            isNext = isEnemyDeath();
        }
    }

    void GenerateWave()
    {
        if (isNext)
        {
            currentTime += Time.deltaTime;

            // Wave間のCoolタイムを調査
            if (currentWave < NextTime.Length && currentTime <= NextTime[currentWave])
                return;

            // 何のために…？
            Vector2 pos = new Vector2(gameObject.transform.position.x, gameObject.transform.position.y);

            // プレハブからインスタンスを生成
            GameObject obj = (GameObject)Instantiate(EnemyList[currentWave], transform.position, Quaternion.identity);

            // 作成したオブジェクトを子として登録
            obj.transform.parent = transform;
            isNext = false;

        }
    }
#endif



    bool isEnemyDeath()
    {
        int enemyCount = gameObject.transform.childCount;

        if(enemyCount==0)
        {
            currentWave++;
            if (currentWave >= EnemyList.Length)
            {
                isClear = true;
            }
            currentTime = 0.0f;
            return true;
        }
        return false;
    }

}
