﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGroup : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if(IsChildrenDead())
        {
            Destroy(this.gameObject);
        }
	}

    public bool IsChildrenDead()
    {
        var components = transform.GetComponentsInChildren<enemyBase>();
        foreach (var component in components)
        {
            if (component.IsDead() == false)
                return false;
        }
        return true;
    }
}
