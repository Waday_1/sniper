﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shot : MonoBehaviour
{
	public Particle particle;
    public float speed;
    public Vector3 moveVec;
    public bool isStrong;
    public enum ShotType
    {
        White,
        Red,
        Bule
    };
    public ShotType type;

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 pos = transform.position;

        pos += moveVec * speed;

        transform.position = pos;
        if(transform.position.z>=10)
        {
            Destroy(this.gameObject);
            if(GameSystem.Instance.IsPlaying)
                GameSystem.Instance.AddScore(-1);
        }
    }

    private void OnCollisionEnter(Collision other)
    {
		
        if (other.gameObject.CompareTag("Enemy"))
        {
			var enemy = other.gameObject.GetComponent<enemyBase>();
			if (enemy.type==(enemyBase.Enemytype)type|| enemy.type==enemyBase.Enemytype.white)
            {
				if (enemy.IsStrong && isStrong)
                {
					
                    Destroy(other.gameObject);
                    Destroy(this.gameObject);
                }
				else if(!enemy.IsStrong)
                {
                    Destroy(other.gameObject);
                    Destroy(this.gameObject);

                    switch(enemy.type)
                    {
                        case enemyBase.Enemytype.white:
					        GameSystem.Instance.AddScore (1000);
                            break;
                        case enemyBase.Enemytype.red:
                        case enemyBase.Enemytype.blue:
                            GameSystem.Instance.AddScore(5000);
                            break;
                    }
                }
                else
                {
                    Destroy(this.gameObject);
                }
				particle.material = other.gameObject.GetComponent<MeshRenderer>().material;
				particle.Burst (transform.position);
				AudioManager.Instance.PlayCheerSE1 ();
            }
            else
            {
                Destroy(this.gameObject);
                GameSystem.Instance.AddScore(-500);
            }
        }
    }
}
