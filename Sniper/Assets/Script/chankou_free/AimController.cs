﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimController : MonoBehaviour {

	Transform aimTrans;
	[SerializeField] GameObject shotLeftPrefab;
	[SerializeField] GameObject shotRightPrefab;
	[SerializeField] GameObject particlePrefab;
	Vector3 screenPoint;
	bool isMouseLeft = false;
	bool isMouseRight = false;
	float timeLeft = 0f;
	float timeRight = 0f;
	[SerializeField] float chargeTime = 0.5f;

	// Use this for initialization
	void Start () {
		aimTrans = this.transform;
	}

	// Update is called once per frame
	void Update () {
		if (isMouseLeft) {
			timeLeft += Time.deltaTime;
		}

		// マウスカーソルについてくる処理
		this.screenPoint = Camera.main.WorldToScreenPoint(aimTrans.position);
		Vector3 pos = new Vector3 (Input.mousePosition.x,Input.mousePosition.y,screenPoint.z);
		aimTrans.position = Camera.main.ScreenToWorldPoint (pos);




		// マウス左を押した時
		if (Input.GetMouseButtonDown(0)){
			isMouseLeft = true;
		}

		// マウス左を離した時
		if (Input.GetMouseButtonUp(0)){
			isMouseLeft = false;

			// SE再生
			AudioManager.Instance.PlayShotSE();

			// 弾を生成
			GameObject shotObj = (GameObject)Instantiate (
				shotLeftPrefab,
				Camera.main.transform.position,
				Quaternion.identity
			);

			// 方向ベクトルを計算
			Vector3 moveVec = Vector3.Normalize (aimTrans.position - Camera.main.transform.position);

			Shot shot = shotObj.GetComponent<Shot> ();
			shot.moveVec = moveVec;

			// パーティクルを生成
			GameObject particleObj = (GameObject)Instantiate (
				particlePrefab,
				Vector3.zero,
				Quaternion.identity
			);
			Particle particle = particleObj.GetComponent<Particle> ();
			shot.particle = particle;

			// チャージ判定
			if (timeLeft >= chargeTime) {
				timeLeft = 0;
				shot.isStrong = true;
			}
		}




		// マウス右を押した時
		if (Input.GetMouseButtonDown(1)){
			isMouseRight = true;
		}

		// マウス右を離した時
		if (Input.GetMouseButtonUp(1)){
			isMouseRight = false;

			// SE再生
			AudioManager.Instance.PlayShotSE();

			// 弾を生成
			GameObject shotObj = (GameObject)Instantiate (
				shotRightPrefab,
				Camera.main.transform.position,
				Quaternion.identity
			);

			// 方向ベクトルを計算
			Vector3 moveVec = Vector3.Normalize (aimTrans.position - Camera.main.transform.position);

			Shot shot = shotObj.GetComponent<Shot> ();
			shot.moveVec = moveVec;

			// パーティクルを生成
			GameObject particleObj = (GameObject)Instantiate (
				particlePrefab,
				Vector3.zero,
				Quaternion.identity
			);
			Particle particle = particleObj.GetComponent<Particle> ();
			shot.particle = particle;

			// チャージ判定
			if (timeRight >= chargeTime) {
				timeRight = 0;
				shot.isStrong = true;
			}
		}
	}
}
