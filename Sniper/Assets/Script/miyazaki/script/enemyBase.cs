﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyBase : MonoBehaviour {
	bool _IsDead = false;

	public bool IsStrong = false;

	public enum Enemytype{
		white,
		red ,
		blue ,
	}

	public Enemytype type;

	protected void SetDead()
	{
		_IsDead = true;
	}

	public bool IsDead()
	{
		return _IsDead;
	}

}
