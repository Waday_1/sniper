﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//往復移動する敵
public class enemy3 : enemyBase {

	public Vector3 StartPos= new Vector3(2.0f, 0.0f, 0.0f);

	public Vector3 EndPos = new Vector3(-2.0f, 0.0f, 0.0f);

	public float time = 5.0f;

	float speed;

	Vector3 moveDir;

	float length;

	float timer;
	float radious;

	// Use this for initialization
	void Start () {

		transform.position = StartPos;

		moveDir = EndPos -  StartPos;

		length = moveDir.magnitude;
		speed = length / time;
		moveDir.Normalize ();
	}
	
	// Update is called once per frame
	void Update () {

		if (IsDead())
			return;

		transform.position += moveDir * Time.deltaTime * speed;
		if ((transform.position - StartPos).magnitude > length) {

			transform.position = EndPos;

			moveDir = StartPos -  EndPos;
			var work = StartPos;
			StartPos = EndPos;
			EndPos = work;

			length = moveDir.magnitude;
			speed = length / time;
			moveDir.Normalize ();
		}

	}
}
