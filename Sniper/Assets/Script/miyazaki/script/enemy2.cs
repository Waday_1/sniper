﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemy2 : enemyBase {
	public Vector3 StartPos= new Vector3(2.0f, 0.0f, 0.0f);

	public Vector3 EndPos = new Vector3(-2.0f, 0.0f, 0.0f);

	public float time = 5.0f;

	float speed;

	Vector3 moveDir;

	float length;

	float timer;
	float radious;

	// Use this for initialization
	void Start () {
		
		transform.position = StartPos;

		moveDir = EndPos -  StartPos;

		length = moveDir.magnitude;
		speed = length / time;
		moveDir.Normalize ();

		timer = 0.0f;
		radious = 0.0f;
	}

	// Update is called once per frame
	void Update () {

		timer += Time.deltaTime;
		radious = timer/time * Mathf.Deg2Rad * 360;

		transform.position = EndPos + new Vector3(length * Mathf.Cos(radious), length * Mathf.Sin(radious), 0.0f);
	}
}
