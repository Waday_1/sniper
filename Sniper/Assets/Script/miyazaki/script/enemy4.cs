﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemy4 : enemyBase {

	public Vector3 StartPos= new Vector3(2.0f, 0.0f, 0.0f);

	public Vector3 EndPos = new Vector3(-2.0f, 0.0f, 0.0f);

	public float stopping_time;
	public Vector3 StoppingPos= new Vector3(0.0f, 0.0f, 0.0f);

	public float time = 5.0f;

	bool is_stopping = false;

	float speed;

	Vector3 moveDir;

	float length;

	float timer;

	// Use this for initialization
	void Start () {

		transform.position = StartPos;

		moveDir = EndPos -  StartPos;

		length = moveDir.magnitude;
		speed = length / time;
		moveDir.Normalize ();

		timer = 0.0f;
	}
	
	// Update is called once per frame
	void Update () {

		if (IsDead())
			return;

		if(transform.position.x > StoppingPos.x -0.1f && transform.position.x < StoppingPos.x + 0.1f &&
			transform.position.y > StoppingPos.y -0.1f && transform.position.y < StoppingPos.y + 0.1f ){
			if (is_stopping == false)
				timer = stopping_time;
			is_stopping = true;
		}

		if(timer<0)
			transform.position += moveDir * Time.deltaTime * speed;


		timer -= Time.deltaTime;
		if (timer < -1.0f)
			timer = -1.0f;

		if ((transform.position - StartPos).magnitude > length) {
			SetDead();
		}


	}
}
