﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeOut : MonoBehaviour {

	public float fSpeed;   //透明化の速さ
	float fAlpha;

	int a;
	// Use this for initialization
	void Start () {
		fAlpha = 0.0f;
	}
	
	// Update is called once per frame
	void Update () {

		if (_Started) {
			
			//アルファ値を足して行く
			fAlpha += fSpeed;

			GetComponent<Image>().color = new Color(0, 0, 0, fAlpha);

			//フェードが終了したら
			if (1.0f <= fAlpha) {
				_FadeEnded = true;
			}
		}
	}

	bool _Started = false;
	bool _FadeEnded = false;

	public bool IsFadeEnded
	{
		get
		{
			return _FadeEnded;
		}
	}

	public void StartFade()
	{
		_Started = true;
	}
}
