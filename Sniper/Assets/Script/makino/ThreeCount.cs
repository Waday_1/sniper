﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ThreeCount : MonoBehaviour {

	enum CountDownState
	{
		Invalid,
		StartCountDown,
		CountDown,
		EndCountDown,
	}

	CountDownState _State = CountDownState.Invalid;

	public Text   ScoreText;          //スコアのテキスト
	public int    nCntInit;           //初期カウントダウン 
    int CurrentCount;
	float         fCntTimer;          //経過時間
	public        float fDownTime;    //カウントダウンの秒数

	public bool IsCountDownEnd {
		get {
			return _State == CountDownState.EndCountDown;
		}
	}

	public bool IsCountDowning {
		get {
			return _State == CountDownState.CountDown || _State == CountDownState.StartCountDown;
		}
	}


	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {

		if(IsCountDowning) 
		{
			fCntTimer += Time.deltaTime;

			//カウンターが一定値を超えたら
			if (fDownTime <= fCntTimer)
			{
				fCntTimer = 0.0f;

                //カウントダウン
                CurrentCount--;

				ScoreText.text = CurrentCount.ToString ();

				//カウントダウンが0になったら
				if (CurrentCount == 0) 
				{
					ScoreText.text = "GO!!";
				}

				//"GO!!"を消す
				if (CurrentCount < 0)
				{
					_State = CountDownState.EndCountDown;
					ScoreText.text = "";
				}
			}
		}
	}

	public void KickCountDown()
	{
        CurrentCount    = nCntInit;
        fCntTimer       = 0.0f;
        ScoreText.text  = CurrentCount.ToString();
        _State          = CountDownState.StartCountDown;
	}
}
