﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Fade : MonoBehaviour
{
	public enum FadeType
	{
		FadeIn,

		FadeOut
	}

	public enum FadeState
	{
		Invalid,
		FadeStart,
		Fade,
		FadeEnd
	}

	FadeType _Fade;

	FadeState _State;

	float fAlpha;

	public float fSpeed;   //透明化の速さ

	public FadeType Type
	{
		get
		{
			return _Fade;
		}
	}

	public bool IsFadeEnded
	{
		get {
			return _State == FadeState.FadeEnd;
		}
	}

	void Update()
	{
		if (_State == FadeState.FadeStart || _State == FadeState.Fade) {

			if (Type == FadeType.FadeIn)
				UpdateFadeIn ();
			else if (Type == FadeType.FadeOut)
				UpdateFadeOut ();
		}
	}


	void UpdateFadeIn () {

		//アルファ値を足して行く
		fAlpha += fSpeed;

		GetComponent<Image>().color = new Color(0, 0, 0, fAlpha);

		//フェードが終了したら
		if (1.0f <= fAlpha) {
			_State = FadeState.FadeEnd;
		}

	}

	void UpdateFadeOut () {
		fAlpha -= fSpeed;

		GetComponent<Image> ().color = new Color (0, 0, 0, fAlpha);

		//フェードが終了したら
		if (fAlpha <= 0.0f) {

			_State = FadeState.FadeEnd;
			fAlpha = 0.0f;
		}
	}

	public void StartFade(FadeType type)
	{
		_Fade = type;
		_State = FadeState.FadeStart;

		if(type == FadeType.FadeIn)
			fAlpha = 0.0f;
		else if(type == FadeType.FadeOut)
			fAlpha = 1.0f;
	}
}

