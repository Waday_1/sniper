﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Clear : MonoBehaviour
{
	public enum AnimationState
	{
		Invalid,
		Start,
		Animation,
		End
	}
		
	public Text text;

    public string clear = "Clear !!";

	AnimationState _State;

	float _Alpha = 0;

	public bool IsFadeEnded
	{
		get {
			return _State == AnimationState.End;
		}
	}

	void Start()
	{
		text = gameObject.GetComponent<Text> ();
	}

	void Update()
	{
		if (_State == AnimationState.Animation || _State == AnimationState.Start)
		{
			_Alpha += Time.deltaTime / 3;
			var alpha = (int)255 * _Alpha;

			text.color = new Color (255, 255, 255, alpha);
			if (1 < _Alpha) {
				text.color = new Color (255, 255, 255, 255);
				_State = AnimationState.End;
			}
		}
	}

	public void SetClear(string header)
	{
        text.text = header + "\n" + clear;

        _State = AnimationState.Start;
		_Alpha = 0;
	}

    public void PlayGame()
    {
        _State = AnimationState.Invalid;
        text.color = new Color(255, 255, 255, 0);
    }
}

