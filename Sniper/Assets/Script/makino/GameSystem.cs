﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SingletonMonoBehaviour<T> : MonoBehaviour where T : MonoBehaviour
{
    private static T instance;
    public static T Instance
    {
        get
        {
            if (instance == null)
            {
                instance = (T)FindObjectOfType(typeof(T));

                if (instance == null)
                {
                    Debug.LogError(typeof(T) + "is nothing");
                }
            }

            return instance;
        }
    }

}

public class GameSystem : SingletonMonoBehaviour<GameSystem>
{
    //シーンの種類
    public enum SystemState
    {
        Invalid = -1,
        Title,
        TitleFadeIn,
        PreThreeCount,
        ThreeCount,
        PreMain,
        Main,
        PreMainClear,
        MainClear,
        NextStage,
        PreResult,
        Result,
        ResultFadeIn,
        PreTitle
    };

    public bool DebugJump = true;

    public GameObject[] StagePrefabList;

    public SystemState _State;

    public int StageIndex = 0;

    public Fade fade;

	public Clear clear;

    public ThreeCount threeCount;

    bool isStage = false;


    GameObject _ActiveStage;

    public bool IsPlaying
    {
        get
        {
            return _State == SystemState.Main;
        }
    }


    bool ThreeCountVisible
    {
        get
        {
            if (threeCount == null)
                return false;
            return threeCount.gameObject.activeSelf;
        }
        set
        {
            if (threeCount == null)
                return;

            threeCount.gameObject.SetActive(value);
        }
    }

    public Score score;

    bool ScoreVisible
    {
        get
        {
            if (score == null)
                return false;
            return score.gameObject.activeSelf;
        }
        set
        {
            if (score == null)
                return;

            score.gameObject.SetActive(value);
        }
    }

	public void Awake()
    {
        if (this != Instance)
        {
            Destroy(this);
            return;
        }
        Screen.SetResolution(1024, 768, false);
        DontDestroyOnLoad(this.gameObject);
    }

    // Use this for initialization
    void Start()
    {
        SceneManager.LoadScene(SystemState.Title.ToString());
    }

    // Update is called once per frame
    void Update()
    {
        if (DebugJump && isStage)
        {
            var index = -1;
            if (Input.GetKey(KeyCode.F1))
            {
                index = 0;
            }
            else if (Input.GetKey(KeyCode.F2))
            {
                index = 1;
            }
            else if (Input.GetKey(KeyCode.F3))
            {
                index = 2;
            }
            else if (Input.GetKey(KeyCode.F4))
            {
                index = 3;
            }
            else if (Input.GetKey(KeyCode.F5))
            {
                index = 4;
            }
            else if (Input.GetKey(KeyCode.F6))
            {
                index = 5;
            }
            else if (Input.GetKey(KeyCode.F7))
            {
                index = 6;
            }
            else if (Input.GetKey(KeyCode.F8))
            {
                index = 7;
            }
            else if (Input.GetKey(KeyCode.F9))
            {
                index = 8;
            }
            else if (Input.GetKey(KeyCode.F10))
            {
                index = 9;
            }

            if (0 <= index)
            {
                StageIndex = index;
                _State = SystemState.PreThreeCount;
                fade.StartFade(Fade.FadeType.FadeOut);
            }
        }

        Transaction();
    }

    private void DestoryActiveScene()
    {
        if (_ActiveStage != null)
            Destroy(_ActiveStage);
    }

    public void AddScore(int value = 1)
    {
        score.AddScore(value);
    }

    void Transaction()
    {
        switch (_State)
        {
            case SystemState.Title:
                {
                    DestoryActiveScene();
                    ThreeCountVisible = false;
                    ScoreVisible = false;
                    StageIndex = 0;

                    //タイトルの状態でクリックされたら
                    if (Input.GetMouseButtonDown(0))
                    {
                        // フェードインを開始
                        fade.StartFade(Fade.FadeType.FadeIn);

                        // タイトルがフェードイン終了を待つ
                        _State = SystemState.TitleFadeIn;
                    }
                }
                break;
            case SystemState.TitleFadeIn:
                {
                    DestoryActiveScene();
                    ThreeCountVisible = false;
                    ScoreVisible = false;

                    // フェードインが終了したら
                    if (fade.IsFadeEnded)
                    {
                        // フェードアウトを開始
                        fade.StartFade(Fade.FadeType.FadeOut);

                        // スリーカウントの準備をする
                        _State = SystemState.PreThreeCount;

                        SceneManager.LoadScene("Stage");
                        isStage = true;
                    }
                }
                break;
            case SystemState.PreThreeCount:
                {
                    DestoryActiveScene();
                    ThreeCountVisible = true;
                    ScoreVisible = true;
                    clear.PlayGame();

                    // フェードアウトが終了したら
                    if (fade.IsFadeEnded)
                    {
                        // カウントダウンを開始する
                        threeCount.KickCountDown();
                        _State = SystemState.ThreeCount;
                    }
                }
                break;
            case SystemState.ThreeCount:
                {
                    DestoryActiveScene();
                    ThreeCountVisible = true;
                    clear.PlayGame();

                    // カウントダウンの終了
                    if (threeCount.IsCountDownEnd)
                        _State = SystemState.PreMain;
                }
                break;
            case SystemState.PreMain:
                {
                    DestoryActiveScene();
                    ThreeCountVisible = false;
                    clear.PlayGame();
                    var finished = true;

                    if (0 <= StageIndex && StageIndex < StagePrefabList.Length)
                    {
                        // 指定されたステージを読み込む
                        var pfb = StagePrefabList[StageIndex];

                        if (pfb != null)
                        {
                            _ActiveStage = Instantiate(pfb);
                            _State = SystemState.Main;
                            finished = false;
                        }
                    }

                    if (finished)
                    {
                        // ステージが終わった
                        fade.StartFade(Fade.FadeType.FadeIn);
                        _State = SystemState.PreResult;
                    }
                }
                break;
            case SystemState.Main:
                {
                    ThreeCountVisible = false;
                    clear.PlayGame();


                    // ステージがクリアした　or 失敗した
                    if (_ActiveStage == null)
                        _State = SystemState.PreResult;
                    else
                    {
                        var manager = _ActiveStage.GetComponent<EnemyManager>();
                        if(manager == null)
                            _State = SystemState.PreResult;
                        else
                        {
                            if (manager.IsClear)
								_State = SystemState.PreMainClear;
                        }
                    }
                }
                break;
            case SystemState.PreMainClear:
                {
                    // クリアを表示する
					clear.SetClear( "Stage:" + (StageIndex + 1).ToString() );

                    // SE
                    if(AudioManager.Instance != null)
                        AudioManager.Instance.PlayCheerSE2();

                    // 遷移
                    _State = SystemState.MainClear;
                }
                break;
            case SystemState.MainClear:
                {
                    ThreeCountVisible = false;

					if (clear.IsFadeEnded)
					{
						//クリア済み状態でクリックされたら
						if (Input.GetMouseButtonDown(0))
							_State = SystemState.NextStage;
					}
	            }
                break;

            case SystemState.NextStage:
                {
                    ++StageIndex;
                    if (StagePrefabList.Length <= StageIndex)
                    {
                        _State = SystemState.PreResult;
                    }
                    else
                    {
                        _State = SystemState.PreThreeCount;
                        clear.PlayGame();
                    }
                }
                break;

            case SystemState.PreResult:
                {
                    clear.PlayGame();
                    isStage = false;
                    _State = SystemState.Result;

                    // フェードインを開始
                    fade.StartFade(Fade.FadeType.FadeIn);

                    // SE
                    if (AudioManager.Instance != null)
                        AudioManager.Instance.PlayCheerSE2();
                }
                break;

            case SystemState.Result:
                {
                    if(fade.IsFadeEnded)
                    {
                        SceneManager.LoadScene("Result");
                        fade.StartFade(Fade.FadeType.FadeOut);

                        // リザルトがフェードイン終了を待つ
                        _State = SystemState.ResultFadeIn;
                    }
                }
                break;

            case SystemState.ResultFadeIn:
                {
                    //リザルトの状態でクリックされたら
                    if (fade.IsFadeEnded && Input.GetMouseButtonDown(0))
                    {
                        _State = SystemState.PreTitle;
                    }
                }
                break;

            case SystemState.PreTitle:
                {
                    // フェードアウトを開始
                    score.ClearScore();
                    SceneManager.LoadScene("Title");

                    _State = SystemState.Title;
                }
                break;
        }
    }
}
