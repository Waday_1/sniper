﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TitleToMain : ChangeScene {

	public FadeOut fadeout;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		//左クリックが行われたら
		if (Input.GetMouseButtonDown (0)) {
			fadeout.StartFade ();
		}	

		if (fadeout.IsFadeEnded) {
			Change(ChangeScene.SceneType.Main);
		}
	}
}
