﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Score : MonoBehaviour {

	Text ScoreText;       //スコアのテキスト
	public static int  nScore;   //現在のスコア

	public bool DebugScore = false;

	// Use this for initialization
	void Start ()
	{
		ScoreText = gameObject.GetComponent<Text> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		//if (DrawScore)
		//{
			//現在の値を文字型に変換し描画
			ScoreText.text = "SCORE:" + nScore.ToString ();

			// デバッグ機能
			if (DebugScore)
			{
				//スペースキーが押されたら
				if (Input.GetKey (KeyCode.Space)) 
				{
					//スコアを加算(実験)
					AddScore (1);
				}
			}
		//}
	}

	public void AddScore(int score)
	{
		nScore += score;
	}

	//スコアを取得
	public int GetScore( )
	{
		return nScore;
	}

	public void ClearScore()
	{
		nScore = 0;
	}
}
