﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour
{
	int a;
	//シーンの種類
	public enum SceneType
	{
		Title,
		Main,
		Result
	};
		
	protected void Change(SceneType type)
	{
		//指定したシーンに遷移
		SceneManager.LoadScene (type.ToString ());
	}
}
