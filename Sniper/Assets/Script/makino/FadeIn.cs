﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeIn : MonoBehaviour {

	public float fSpeed;   //透明化の速さ
	float fAlpha;
	public bool  bFadeFlg;

	// Use this for initialization
	void Start () {
		fAlpha = 1.0f;
		bFadeFlg = false;
	}

	// Update is called once per frame
	void Update () {

		if (bFadeFlg == false) {

			fAlpha -= fSpeed;

			GetComponent<Image> ().color = new Color (0, 0, 0, fAlpha);

			//フェードが終了したら
			if (fAlpha <= 0.0f) {
				
				bFadeFlg = true;
				fAlpha = 0.0f;
			}
		}
	}
}
